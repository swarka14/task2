﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime CreatedOn { get; }
        public decimal Sum { get; }
        public string CarId { get; }
        public TransactionInfo(DateTime createdOn, decimal sum, string carId)
        {
            CreatedOn = createdOn;
            Sum = sum;
            CarId = carId;
        }
    }
}