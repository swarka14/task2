﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
   public static class Settings
    {
        public static int ChargingTimeout { get; }
        public static int ParkingSpace { get; }
        public static int TransactionLoggingTimeout { get; }
        public static decimal Fine { get; }
        public static Dictionary<VehicleType, decimal> Prices = new Dictionary<VehicleType, decimal>
        {
            [VehicleType.PassengerCar] = 2,
            [VehicleType.Truck] = 5,
            [VehicleType.Bus] = 3.5m,
            [VehicleType.Motorcycle] = 1
        };

        static Settings()
        {
            //Milliseconds
            TransactionLoggingTimeout = 5000;
            ChargingTimeout = 60000;
            ParkingSpace = 10;
            Fine = 2.5m;
        }

    }
}