﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public static List<Vehicle> Cars = new List<Vehicle>(Settings.ParkingSpace);
        public ReadOnlyCollection<Vehicle> Vehicles = new ReadOnlyCollection<Vehicle>(Cars);
        private static readonly Parking instance = new Parking();
        public TransactionInfo[] transactions = new TransactionInfo[] { };

        private Parking()
        { Balance = 0; }

        public static Parking GetInstance()
        {
            return instance;
        }

    }
}