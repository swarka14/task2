﻿using System;
using System.Collections.Generic;
using System.Text;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL
{
    class Program
    {
        private static Menu menu;

        public static void Main(string[] args)
        {

            menu = new Menu();
            menu.MainMenu();
            menu.vehicle = new Vehicle("", VehicleType.PassengerCar, 30);
        }

    }
}
