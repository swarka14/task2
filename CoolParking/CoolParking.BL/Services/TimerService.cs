﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Dynamic;
using System.Timers;
using System;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public double Interval { get; set; }
        public event ElapsedEventHandler Elapsed;
        private Timer timer;
        public TimerService(double interval)
        {
            Interval = interval;
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);

        }

        public void Start()
        {
            timer = new Timer(Interval) { AutoReset = true };

            timer.Elapsed += OnTimedEvent;

            timer.Enabled = true;
            timer.AutoReset = true;
        }

      private void OnTimedEvent(object sender, ElapsedEventArgs e)
       {
          FireElapsedEvent();
       }

       public void Stop()
        {
          timer.Enabled = false;
       }
        public void Dispose()
        {
            timer.Dispose();
        }
    }
    }
