﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private readonly string _logPath;
        string ILogService.LogPath => _logPath;

        public LogService(string logPath)
        {
            _logPath = logPath;

        }
        public string Read()
        {
            try
            {
                using StreamReader sr = new StreamReader(_logPath);
                Console.WriteLine(sr.ReadToEndAsync());
            }
            catch (UnauthorizedAccessException uae) { Console.WriteLine(uae.Message); }
            catch (ArgumentNullException ane) { Console.WriteLine(ane.Message); }
            catch (ArgumentException ae) { Console.WriteLine(ae.Message); }
            catch (PathTooLongException ptle) { Console.WriteLine(ptle.Message); }
            catch (DirectoryNotFoundException dnfe) { Console.WriteLine(dnfe.Message); }
            catch (NotSupportedException nse) { Console.WriteLine(nse.Message); }

            return "" ;
        }

        public async void Write(string logInfo)
        {
            if (File.Exists("Transaction.log"))
            try
            {
                    using StreamWriter sw = new StreamWriter(_logPath);
                    await sw.WriteLineAsync(logInfo);
            }
            
            catch (ArgumentNullException ane)
            {
                Console.WriteLine(ane.Message);
            }
            catch (ArgumentException ae)
            {
                Console.WriteLine(ae.Message);
            }
            catch (FileNotFoundException fnfe)
            {
                Console.WriteLine(fnfe.Message);
            }
            catch (DirectoryNotFoundException dnfe)
            {
                Console.WriteLine(dnfe.Message);
            }
            catch (IOException ioe)
            {
                Console.WriteLine(ioe.Message);
            }
        }
    }
}