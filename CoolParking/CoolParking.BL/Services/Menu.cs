﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Services
{
   public class Menu
    {
        public Vehicle vehicle;

            public void MainMenu()
            {
            int menuChoice = 0;
            TimerService withdrawTimer = new TimerService(Settings.ChargingTimeout);
            TimerService logTimer = new TimerService(Settings.TransactionLoggingTimeout);
            
            ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, new LogService("D:/Всякое/bsa20-dotnet-hw2-template/CoolParking/CoolParking.BL/Transactions.log"));

            do
                {
                    menuChoice = ShowMenu();
                    if (menuChoice != 0)
                    {
                        ProcessMenuChoice(menuChoice,parkingService,vehicle);
                    }
                } while (menuChoice != 0);

            }

            private int ShowMenu()
            {
                int choice = 0;
                Console.WriteLine("Parking menu:");
                Console.WriteLine("1. Add/Remove car.");
                Console.WriteLine("2. Load parking balance.");
                Console.WriteLine("3.Number of free powers in the parking lot.");
                Console.WriteLine("4. Earned funds for the current period.");
                Console.WriteLine("5. Transaction history.");
                Console.WriteLine("6. Parking vacant spots.");
                Console.WriteLine("7. Show last transaction.");
                Console.WriteLine("0. Exit.");

                choice = Convert.ToInt32(Console.ReadLine());
           
            return choice;
            }

            private void ProcessMenuChoice(int menuChoice, ParkingService service,Vehicle vehicle)
            {
                switch (menuChoice)
                {
                    case 1:
                        Console.WriteLine();
                        Console.WriteLine("Please choose:");
                        Console.WriteLine("1. Add car.\n2. Remove car.");
                        int addRemoveFlag = Convert.ToInt32(Console.ReadLine());
                        if (addRemoveFlag == 1)
                        {
                            service.AddVehicle(vehicle);
                        service.GetVehicles();
                        }
                        else if (addRemoveFlag == 2)
                        {
                        service.RemoveVehicle(vehicle.Id);
                        service.GetCapacity();
                        }
                        break;
                    case 2:
                        Console.WriteLine("Load parking balance:");
                    service.GetBalance();
                        break;
                    case 3:
                        Console.WriteLine("Number of free powers in the parking lot: ");
                    service.GetCapacity();
                        break;
                    case 4:
                        Console.WriteLine("Earned funds for the current period:");
                  
                        break;
                    case 5:
                        Console.WriteLine("Transaction history:");
                    service.ReadFromLog();
                        break;
                    case 6:
                        Console.WriteLine("Parking vacant spots:");
                    service.GetFreePlaces();
                        break;
                    case 7:
                        Console.WriteLine("Last transaction: ");
                    service.GetLastParkingTransactions();
                        break;
                }
            }

        }
 
}
