﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using System.Collections.ObjectModel;
using CoolParking.BL.Models;
using System.Collections.Generic;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly Parking parking;


        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logService = logService;
            _logTimer = logTimer;
            parking = Parking.GetInstance();


            _withdrawTimer.Elapsed += ChargeVehicles;
           _withdrawTimer.Start();
            _logTimer.Elapsed += LogginTransaction;
            _logTimer.Start();

        }

        private void LogginTransaction(object sender, ElapsedEventArgs e)
        {
            ReadFromLog();
        }

        public void Dispose()
        {
          
        }

        public void AddVehicle(Vehicle vehicle)
        {
            
           try
            {
                Parking.Cars.Add(vehicle);
                foreach (Vehicle car in Parking.Cars)
                {
                    Console.WriteLine(car);
                }
               
            }
            catch (IndexOutOfRangeException )
            {
                Console.WriteLine("User entered non-existing array index!");
            }
            catch (FormatException)
            {
                Console.WriteLine("User entered not number!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }

        }
        public  decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingSpace;
        }

        public int GetFreePlaces()
        {
           return Parking.Cars == null ? Settings.ParkingSpace : Settings.ParkingSpace - Parking.Cars.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return parking.transactions;
        }

        public void ChargeVehicles(object sender, ElapsedEventArgs e)
        {
            

            foreach (var car in Parking.Cars)
            {
                decimal fee = Settings.Prices[car.VehicleType];
                decimal feewithFine = fee * Settings.Fine;
                if (car.Balance > 0)
                {
                    MakeTransaction(car, FineifBalanceLessThanTariff(car, fee));

                } else
                {
                    MakeTransaction(car, FineifBalanceLessThanTariff(car, feewithFine));
                }
                SaveTransactionInfo(car.Balance > 0 ? fee : feewithFine, car); 
            } 
            
        }
        private void SaveTransactionInfo(decimal fee, Vehicle car)
        {
            for (int i = 0; i < parking.transactions.Length; i++)
            {
                parking.transactions[i] = new TransactionInfo(new DateTime(), fee, car.Id);
            }
        }
        private decimal FineifBalanceLessThanTariff(Vehicle car,decimal fee)
        {
            if(car.Balance < fee)
            {
                decimal diff = fee - car.Balance;
                decimal charge = car.Balance+diff*Settings.Fine;
                return charge;
            }
            return fee;
        }

        private void MakeTransaction(Vehicle car, decimal fee)
        {
            car.Balance -= fee;
            parking.Balance += fee;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.Vehicles;
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }


        public void RemoveVehicle(string vehicleId)
        {
            if (Parking.Cars.Exists(c => c.Id == vehicleId))
            {
                throw new InvalidOperationException("Here don`t have this car");
            }

            if (Parking.Cars.Find(c => c.Id == vehicleId).Balance < 0)
            {
                throw new InvalidOperationException("You must refill your balance");
            }
            else try
            {
                Parking.Cars.Remove(Parking.Cars.Find(c => c.Id == vehicleId));
            }
            catch (IndexOutOfRangeException )
            {
                Console.WriteLine("User entered non-existing array index!");
            }
            catch (FormatException)
            {
                Console.WriteLine("User entered not number!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception: {e.Message}");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Parking.Cars.Find(c => c.Id == vehicleId).Balance += sum;
        }
    }
}