﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Transactions")]
    public class TransactionController : Controller
    {
       
        private readonly IParkingService _service;
        public TransactionController(IParkingService service)
        {
            _service = service;
        }
        // GET: api/Transaction/all
        [HttpGet("all")]
        public ActionResult<TransactionInfo> Log()
        {
            return Json(_service.ReadFromLog());
        }

        // GET: api/Transaction/last
        [HttpGet("last")]
        public ActionResult<TransactionInfo> GetLastTransaction()
        {
            return Json(Parking.GetInstance().transactions);
        }

        //PUT: api/transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public JsonResult TopUpVehicle(string id, decimal money)
        {
            try
            {
                _service.TopUpVehicle(id, money);
                return Json(Ok());
            }
            catch (Exception)
            {
                return Json(BadRequest());
            }
        }

    }
}
