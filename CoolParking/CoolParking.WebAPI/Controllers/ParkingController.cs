﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;


namespace CoolParking.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Parking")]
    public class ParkingController : Controller
    {
        
        private readonly IParkingService _service;
        public ParkingController(IParkingService service)
        {
            _service = service;
        }
        // GET: api/Parking
        [HttpGet("capsity")]

        public JsonResult Capasity()
        {
            return Json(_service.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public JsonResult FreePlaces()
        {
            return Json(_service.GetFreePlaces());
        }

        [HttpGet("balance")]
        public JsonResult Balance()
        {
            return base.Json(Parking.GetInstance().Balance);
        }
    }
}