﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        private readonly IParkingService _service;

        readonly Vehicle vehicle = new Vehicle("GP-5263-GC", VehicleType.Bus, 196.5m);
        public VehiclesController(IParkingService service)
        {
            _service = service;
        }
        // GET: api/Vehicles/
        [HttpGet]
        public JsonResult Vehicles()
        {
            try
            {
                return Json(Parking.GetInstance().Vehicles.Select(x => new { x.Id, type = x.VehicleType.ToString() }));
            }
            catch (Exception)
            {
                return Json(NoContent());
            }
        }

        // GET: api/Vehicles/id
        [HttpGet("id")]
        public JsonResult GetId(string id)
        {
            if (vehicle.Id != @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}")
                return Json(BadRequest());
            if (vehicle == null)
                return Json(NotFound());
            else
            {
                return Json(Parking.GetInstance().Vehicles.Select(x => x.Id == id));
            }
        }

        // POST: api/Vehicles

        [HttpPost]
        public JsonResult AddCar([FromBody]Vehicle vehicle)
        {
            try
            {
               _service.AddVehicle(vehicle);
                if (vehicle.Id != null)
                    return Json(Ok());
                else
                    return Json(BadRequest());
            }
            catch (Exception)
            {
                return Json(NotFound());
            }
        }



        // DELETE: api/ApiWithActions/id
        [HttpDelete("id")]
        public JsonResult Delete(string id)
        {
            try
            {
                _service.RemoveVehicle(id);
                if (vehicle==null)
                    return Json(BadRequest());
                else
                return Json(Ok(NoContent()));
            }
            catch (Exception)
            {
                return Json(NoContent());
            }
        }
    } 
}
